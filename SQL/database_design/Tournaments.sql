CREATE TABLE Tournaments (
    TournamentLink VARCHAR,
    TournamentID   VARCHAR PRIMARY KEY,
    Surface        VARCHAR,
    PrizeMoney     BIGINT,
    Location       VARCHAR,
    DateFrom       DATE,
    DateTo         DATE
);