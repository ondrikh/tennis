CREATE TABLE MatchLinks (
    MatchLink    VARCHAR PRIMARY KEY
                         
    TournamentID VARCHAR,
    MatchID      VARCHAR
)
WITHOUT ROWID;



CREATE UNIQUE INDEX "" ON MatchLinks (
    MatchLink
);